import 'point_model.dart';

class TaskModel {
  final String id;
  List<List<String>> field;
  final Point start;
  final Point end;

  TaskModel(
      {this.id = '',
      required this.field,
      required this.start,
      required this.end});

  factory TaskModel.fromJson(Map<String, dynamic> json) {
    return TaskModel(
      id: json['id'],
      field: List<List<String>>.from(json['field'].map((row) => row.split(''))),
      start: Point(json['start']['x'] as int, json['start']['y'] as int),
      end: Point(json['end']['x'] as int, json['end']['y'] as int),
    );
  }

  List<Point> transformField() {
    List<Point> points = [];
    for (var y = 0; y < field.length; y++) {
      for (var x = 0; x < field[y].length; x++) {
        final text = field[y];
        if (text[x] != '.') {
          points.add(Point(x, y));
        }
      }
    }
    return points;
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'field': field,
      'start': {'x': start.x, 'y': start.y},
      'end': {'x': end.x, 'y': end.y},
    };
  }
}

import 'point_model.dart';

class ResultModel {
  final String id;
  List<Point> path;

  ResultModel({
    required this.id,
    required this.path,
  });

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'result': {
        'path': path.join('->'),
        'steps': path
            .map((Point point) => {
                  'x': point.x.toString(),
                  'y': point.y.toString(),
                })
            .toList(),
      }
    };
  }
}

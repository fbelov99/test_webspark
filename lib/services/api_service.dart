import 'dart:convert';

import 'package:http/http.dart';
import 'package:test_webspark/models/result_model.dart';

import '../models/task_model.dart';

class ApiRequests {
  ApiRequests._privateConstructor();

  static final ApiRequests _instance = ApiRequests._privateConstructor();

  static ApiRequests get instance => _instance;

  Future<List<TaskModel>> getTasks(String url) async {
    try {
      final response = await get(Uri.parse(url));

      if (response.statusCode == 200) {
        List<dynamic> data = jsonDecode(response.body)['data'] as List<dynamic>;

        return data
            .map((e) => TaskModel.fromJson(e as Map<String, dynamic>))
            .toList();
      } else {
        throw 'Request code: ${response.statusCode}. ${jsonDecode(response.body)['error']}';
      }
    } catch (e) {
      throw 'Error: $e';
    }
  }

  Future<bool> sendTasks(String url, List<ResultModel> results) async {
    try {
      final response = await post(Uri.parse(url),
          body: jsonEncode(results.map((e) => e.toJson()).toList()),
          headers: {'Content-Type': 'application/json'});

      if (response.statusCode == 200) {
        List<dynamic> data = jsonDecode(response.body)['data'] as List<dynamic>;

        return data
            .map((e) => e['correct'] as bool)
            .reduce((value, element) => value & element);
      } else {
        throw 'Request code: ${response.statusCode}. ${jsonDecode(response.body)['error']}';
      }
    } catch (e) {
      throw 'Error: $e';
    }
  }
}

import 'package:flutter/material.dart';
import 'package:test_webspark/process_screen/process_view.dart';

class HomeView extends StatefulWidget {
  const HomeView({super.key});

  @override
  State<HomeView> createState() => _HomeViewState();
}

class _HomeViewState extends State<HomeView> {
  TextEditingController urlController = TextEditingController();
  final formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: FocusScope.of(context).unfocus,
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.blueAccent,
          title:
              const Text('Home Screen', style: TextStyle(color: Colors.white)),
        ),
        body: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 12, vertical: 20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                'Set valid API base URL in order to continue',
                style: Theme.of(context).textTheme.bodyLarge,
              ),
              Row(
                children: [
                  const Icon(Icons.compare_arrows_outlined,
                      size: 25, color: Colors.grey),
                  const SizedBox(width: 20),
                  Expanded(
                    child: Form(
                      key: formKey,
                      child: TextFormField(
                        controller: urlController,
                        validator: (value) {
                          final url = Uri.tryParse(value ?? '');
                          if (url?.hasAbsolutePath == false) {
                            return 'Please enter valid url';
                          }
                          return null;
                        },
                      ),
                    ),
                  ),
                ],
              ),
              Expanded(
                child: Align(
                  alignment: Alignment.bottomCenter,
                  child: InkWell(
                    onTap: () {
                      if (formKey.currentState!.validate()) {
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (context) =>
                                ProcessView(url: urlController.text)));
                      } else {
                        ScaffoldMessenger.of(context).showSnackBar(
                            const SnackBar(content: Text('Enter valid url')));
                      }
                    },
                    child: Container(
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(15),
                          border: Border.all(color: Colors.black),
                          color: Colors.lightBlueAccent),
                      child: SizedBox(
                        height: 50,
                        child: Center(
                          child: Text(
                            'Start counting process',
                            style: Theme.of(context).textTheme.bodyLarge,
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}

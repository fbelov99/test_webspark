import 'package:flutter/material.dart';
import 'package:test_webspark/models/point_model.dart';
import 'package:test_webspark/models/result_model.dart';
import 'package:test_webspark/models/task_model.dart';
import 'package:test_webspark/result_list_screen/results_view.dart';
import 'package:test_webspark/services/api_service.dart';
import 'dart:collection';

class ProcessView extends StatefulWidget {
  final String url;
  const ProcessView({super.key, required this.url});

  @override
  State<ProcessView> createState() => _ProcessViewState();
}

class _ProcessViewState extends State<ProcessView> {
  int progress = 0;
  String error = '';
  bool isBusy = false;
  List<ResultModel> results = [];
  List<TaskModel> tasks = [];

  @override
  void initState() {
    getTasks();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blueAccent,
        title:
            const Text('Process Screen', style: TextStyle(color: Colors.white)),
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 12, vertical: 20),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              progress != 100
                  ? 'Calculating...'
                  : 'All calculations has finished, you can send your results to server',
              style: Theme.of(context).textTheme.titleLarge,
              textAlign: TextAlign.center,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 20),
              child: Text(
                '$progress%',
                style: Theme.of(context).textTheme.titleLarge,
              ),
            ),
            SizedBox(
                width: 150,
                height: 150,
                child: CircularProgressIndicator(
                    value: progress == 0 ? null : progress / 100)),
            if (error.isNotEmpty)
              Text(
                error,
                style: Theme.of(context).textTheme.bodyLarge,
              ),
            Expanded(
              child: Align(
                alignment: Alignment.bottomCenter,
                child: InkWell(
                  onTap: () async {
                    if (!isBusy /*  && error.isEmpty */) {
                      await sendResult();
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (context) =>
                              ResultsView(tasks: tasks, results: results)));
                      // } else if (!isBusy) {
                      //   getTasks();
                    }
                  },
                  child: Container(
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(15),
                        border: Border.all(color: Colors.black),
                        color: Colors.lightBlueAccent),
                    child: SizedBox(
                      height: 50,
                      child: Center(
                        child: Text(
                          'Send results to server',
                          style: Theme.of(context).textTheme.bodyLarge,
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  Future<void> getTasks() async {
    setState(() {
      isBusy = true;
      error = '';
      progress = 0;
    });
    try {
      tasks = await ApiRequests.instance.getTasks(widget.url);
      calculations(tasks, results);
    } catch (e) {
      error = e.toString();
      setState(() {
        isBusy = false;
      });
    }
  }

  Future<void> sendResult() async {
    setState(() {
      isBusy = true;
      error = '';
    });
    try {
      bool correct = await ApiRequests.instance.sendTasks(widget.url, results);
      if (!correct) {
        error = 'Some results were not correct';
        ScaffoldMessenger.of(context)
            .showSnackBar(SnackBar(content: Text(error)));
      }
    } catch (e) {
      error = e.toString();
    }
    setState(() {
      isBusy = false;
    });
  }

  void calculations(List<TaskModel> tasks, List<ResultModel?> results) {
    progress++;
    for (var task in tasks) {
      ResultModel result = ResultModel(id: task.id, path: [task.start]);

      List<Point>? points = move(task.field, task.start, task.end);
      result.path = points ?? [task.start, task.end];
      results.add(result);
      progress = progress + (100 ~/ tasks.length);
      if (progress > 100) {
        progress = 100;
      }
      setState(() {});
    }
    setState(() {
      isBusy = false;
    });
  }

  List<Point>? move(List<List<String>> field, Point start, Point goal) {
    List<Point> directions = [
      Point(0, 1),
      Point(1, 0),
      Point(0, -1),
      Point(-1, 0),
      Point(1, 1),
      Point(1, -1),
      Point(-1, 1),
      Point(-1, -1)
    ];

    int rowsAmount = field[0].length;
    int columnsAmount = field.length;

    List<List<bool>> visited = List.generate(
      columnsAmount,
      (_) => List.generate(rowsAmount, (_) => false),
    );

    Queue<List<Point>> queue = Queue();
    queue.add([start]);
    visited[start.y][start.x] = true;

    while (queue.isNotEmpty) {
      List<Point> path = queue.removeFirst();
      Point current = path.last;

      if (current.x == goal.x && current.y == goal.y) {
        return path;
      }

      for (Point direction in directions) {
        int newX = current.x + direction.x;
        int newY = current.y + direction.y;
        if (newX >= 0 &&
            newY >= 0 &&
            newX < rowsAmount &&
            newY < columnsAmount &&
            !visited[newY][newX] &&
            field[newY][newX] != 'X') {
          visited[newY][newX] = true;
          List<Point> newPath = List.from(path)..add(Point(newX, newY));
          queue.add(newPath);
        }
      }
    }

    return null;
  }
}

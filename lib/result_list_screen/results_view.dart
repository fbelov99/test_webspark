import 'package:flutter/material.dart';
import 'package:test_webspark/models/result_model.dart';
import 'package:test_webspark/models/task_model.dart';
import 'package:test_webspark/result_list_screen/preview_screen/preview_view.dart';

class ResultsView extends StatelessWidget {
  final List<ResultModel> results;
  final List<TaskModel> tasks;
  const ResultsView({super.key, required this.results, required this.tasks});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blueAccent,
        title:
            const Text('Results Screen', style: TextStyle(color: Colors.white)),
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(vertical: 20),
        child: ListView.separated(
          separatorBuilder: (context, index) => const Divider(),
          itemCount: results.length,
          itemBuilder: (context, i) => ListTile(
            title: Text(
              results[i].path.join('->'),
              textAlign: TextAlign.center,
              style: Theme.of(context).textTheme.bodyLarge,
            ),
            onTap: () => Navigator.of(context).push(MaterialPageRoute(
                builder: (context) =>
                    PreviewView(task: tasks[i], result: results[i]))),
          ),
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:test_webspark/models/result_model.dart';
import 'package:test_webspark/models/task_model.dart';

import '../../models/point_model.dart';

class PreviewView extends StatelessWidget {
  final ResultModel result;
  final TaskModel task;
  const PreviewView({super.key, required this.result, required this.task});

  @override
  Widget build(BuildContext context) {
    int gridLength = task.field.first.length;
    Size screen = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blueAccent,
        title:
            const Text('Preview Screen', style: TextStyle(color: Colors.white)),
      ),
      body: Column(
        children: [
          Container(
            width: screen.width,
            height: screen.width,
            color: Colors.black,
            child: GridView.builder(
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: gridLength,
                  crossAxisSpacing: 3,
                  mainAxisSpacing: 3,
                ),
                itemCount: gridLength * gridLength,
                itemBuilder: (context, index) {
                  int x = index % gridLength;
                  int y = index ~/ gridLength;

                  Color bgColor = checkBgColor(x, y);

                  return Container(
                      color: bgColor,
                      alignment: Alignment.center,
                      child: Text(
                        '($x, $y)',
                        style: TextStyle(
                            color: bgColor == Colors.black
                                ? Colors.white
                                : Colors.black),
                      ));
                }),
          ),
          const SizedBox(height: 12),
          Text(result.path.join('->'),
              style: Theme.of(context).textTheme.titleLarge)
        ],
      ),
    );
  }

  Color checkBgColor(int x, int y) {
    Color bgColor;

    List<Point> pointedFields = task.transformField();

    if (x == task.start.x && y == task.start.y) {
      bgColor = const Color(0xFF64FFDA);
    } else if (x == task.end.x && y == task.end.y) {
      bgColor = const Color(0xFF009688);
    } else if (result.path.any((e) => e.x == x && e.y == y)) {
      bgColor = const Color(0xFF4CAF50);
    } else if (pointedFields.any((e) => e.x == x && e.y == y)) {
      bgColor = Colors.black;
    } else {
      bgColor = Colors.white;
    }
    return bgColor;
  }
}
